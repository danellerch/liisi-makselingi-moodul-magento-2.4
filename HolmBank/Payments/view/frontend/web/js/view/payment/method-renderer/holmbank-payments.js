define([
    'jquery',
    'Magento_Checkout/js/view/payment/default',
    'mage/url',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/full-screen-loader'
], function ($, Component, url, customerData, errorProcessor, fullScreenLoader) {
    'use strict';

    return Component.extend({
        redirectAfterPlaceOrder: false,
        defaults: {
            template: 'HolmBank_Payments/payment/holmbank-payments'
        },
        afterPlaceOrder: function () {
            var redirectUrl = url.build('holmbank/payment/create');
            $.ajax({
                url: redirectUrl,
                type: 'GET',
                async: false,
            }).done(function (response) {
                customerData.invalidate(['cart']);
                window.location.href = response.redirectUrl;
            }).fail(function (response) {
                errorProcessor.process(response, this.messageContainer);
            }).always(function () {
                fullScreenLoader.stopLoader();
            });
        }
    });
});


