<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model;

use HolmBank\Payments\Api\Data\HolmbankOrderInterface;
use HolmBank\Payments\Model\ResourceModel\HolmBankOrder as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class HolmBankOrder extends AbstractModel implements HolmbankOrderInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'holmbank_payment_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
    /**
     * Getter for HolmbankOrderId.
     *
     * @return string|null
     */
    public function getHolmbankOrderId(): ?string
    {
        return $this->getData(self::HOLMBANK_ORDER_ID) === null ? null
            : (string)$this->getData(self::HOLMBANK_ORDER_ID);
    }

    /**
     * Setter for HolmbankOrderId.
     *
     * @param string|null $holmbankOrderId
     *
     * @return void
     */
    public function setHolmbankOrderId(?string $holmbankOrderId): void
    {
        $this->setData(self::HOLMBANK_ORDER_ID, $holmbankOrderId);
    }

    /**
     * Getter for CustomerId.
     *
     * @return int|null
     */
    public function getCustomerId(): ?int
    {
        return $this->getData(self::CUSTOMER_ID) === null ? null
            : (int)$this->getData(self::CUSTOMER_ID);
    }

    /**
     * Setter for CustomerId.
     *
     * @param int|null $customerId
     *
     * @return void
     */
    public function setCustomerId(?int $customerId): void
    {
        $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Getter for OrderId.
     *
     * @return int|null
     */
    public function getOrderId(): ?int
    {
        return $this->getData(self::ORDER_ID) === null ? null
            : (int)$this->getData(self::ORDER_ID);
    }

    /**
     * Setter for OrderId.
     *
     * @param int|null $orderId
     *
     * @return void
     */
    public function setOrderId(?int $orderId): void
    {
        $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Setter for RequestId.
     *
     * @param int|null $orderId
     *
     * @return void
     */
    public function setRequestId(string $requestId): void
    {
        $this->setData(self::REQUEST_ID, $requestId);
    }

    public function getRequestId(): string
    {
        return $this->getData(self::REQUEST_ID);
    }

    /**
     * Getter for OrderStatus.
     *
     * @return string|null
     */
    public function getOrderStatus(): ?string
    {
        return $this->getData(self::ORDER_STATUS);
    }

    /**
     * Setter for OrderStatus.
     *
     * @param string|null $orderStatus
     *
     * @return void
     */
    public function setOrderStatus(?string $orderStatus): void
    {
        $this->setData(self::ORDER_STATUS, $orderStatus);
    }
}
