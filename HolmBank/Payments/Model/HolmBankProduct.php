<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model;

use HolmBank\Payments\Api\Data\HolmbankProductInterface;
use HolmBank\Payments\Model\ResourceModel\HolmBankOrder as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class HolmBankProduct extends AbstractModel implements HolmbankProductInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'holmbank_products_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    public function getHolmBankProductType(): ?string
    {
        return $this->getData(self::TYPE) === null ? null
            : (string)$this->getData(self::TYPE);
    }

    public function setHolmBankProductType(?string $productType): void
    {
        $this->setData(self::TYPE, $productType);
    }

    public function getHolmBankProductName(): ?string
    {
        return $this->getData(self::NAME) === null ? null
            : (string)$this->getData(self::NAME);
    }

    public function setHolmBankProductName(?string $productName): void
    {
        $this->setData(self::NAME, $productName);
    }

    public function getHolmBankProductLogo(): ?string
    {
        return $this->getData(self::LOGO) === null ? null
            : (string)$this->getData(self::LOGO);
    }
    public function setHolmBankProductLogo(?string $productLogo): void
    {
        $this->setData(self::LOGO, $productLogo);
    }
}
