<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Asset\Repository;
use Psr\Log\LoggerInterface;

class PaymentConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Repository
     */
    protected Repository $_assetRepo;
    private HolmbankProductRepository $holmbankProductRepository;
    private ScopeConfigInterface $scopeConfig;
    private LoggerInterface $logger;

    /**
     * Construct function.
     *
     * @param Repository $assetRepo
     */
    public function __construct(
        HolmbankProductRepository $holmbankProductRepository,
        LoggerInterface           $logger,
        ScopeConfigInterface      $scopeConfig,
        Repository                $assetRepo
    )
    {
        $this->holmbankProductRepository = $holmbankProductRepository;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->_assetRepo = $assetRepo;
    }

    /**
     * Payment config provider.
     *
     * @return array[]
     */
    public function getConfig(): array
    {
        $productCode = $this->scopeConfig->getValue('payment/holm_partner/products');
        try {
            $product = $this->holmbankProductRepository->getByProductType($productCode);
            $logoUrl = $product->getHolmBankProductLogo();
            if (empty($logoUrl)) {
                $logoUrl = $this->_assetRepo->getUrl('HolmBank_Payments::images/logo.png');
            }
            return [
                'payment' => [
                    'liisi' => [
                        'isActive' => true,
                        'paymentAcceptanceMarkSrc' => $logoUrl
                    ]
                ],
            ];
        } catch (NoSuchEntityException $e) {
            $this->logger->error($e->getMessage());
        }
        return [
            'payment' => [
                'liisi' => [
                    'isActive' => true,
                    'paymentAcceptanceMarkSrc' => $this->_assetRepo->getUrl('HolmBank_Payments::images/logo.png')
                ]
            ],
        ];
    }
}
