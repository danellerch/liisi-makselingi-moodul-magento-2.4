<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model;

use HolmBank\Payments\Api\Data\HolmbankProductInterface;
use HolmBank\Payments\Api\HolmbankProductRepositoryInterface;
use HolmBank\Payments\Model\ResourceModel\HolmBankProduct;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class HolmbankProductRepository implements HolmbankProductRepositoryInterface
{

    private HolmBankProduct $resource;

    private HolmBankProductFactory $productFactory;

    public function __construct(
        HolmBankProduct $resource,
        HolmBankProductFactory $orderFactory
    ) {
        $this->resource = $resource;
        $this->productFactory = $orderFactory;
    }

    public function clearProducts(): void
    {
        $connection = $this->resource->getConnection();
        $tableName = $this->resource->getMainTable();
        $connection->truncateTable($tableName);
    }

    public function save(HolmbankProductInterface $product): void
    {
        try {
            $this->resource->save($product);
        } catch (AlreadyExistsException|\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getByProductType($productType): HolmbankProductInterface
    {
        $product = $this->productFactory->create();
        $this->resource->load($product, $productType, 'type');
        if (!$product->getId()) {
            throw new NoSuchEntityException(__('Product with type "%1" doesn\'t exist.', $productType));
        }
        return $product;
    }
}
