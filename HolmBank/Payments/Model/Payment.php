<?php

namespace HolmBank\Payments\Model;

use Magento\Payment\Model\Method\AbstractMethod;

class Payment extends AbstractMethod
{
    /**
     * Payment success status code
     *
     * @var string
     */
    const APPROVED_STATUS = 'APPROVED';

    /**
     * Payment rejected status code
     *
     * @var string
     */
    const REJECTED_STATUS = 'REJECTED';

    /**
     * Payment rejected status code
     *
     * @var string
     */
    const PENDING_STATUS = 'PENDING';
}
