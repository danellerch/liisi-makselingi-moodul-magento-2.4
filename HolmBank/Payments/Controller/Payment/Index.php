<?php

declare(strict_types=1);

namespace HolmBank\Payments\Controller\Payment;

use HolmBank\Payments\Model\HolmbankOrderRepository;
use HolmBank\Payments\Model\Payment;
use Laminas\Http\Response;
use Magento\Checkout\Controller\Action;
use Magento\Sales\Model\Order;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

class Index extends Action implements CsrfAwareActionInterface
{
    /**
     * @var PageFactory
     */
    private PageFactory $resultPageFactory;

    private Rejected $rejected;


    private HolmbankOrderRepository $holmbankOrderRepository;
    private Json $json;
    private JsonFactory $jsonResultFactory;
    private OrderRepositoryInterface $orderRepository;
    private LoggerInterface $logger;


    /**
     * Class constructor.
     *
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     * @param PageFactory $resultPageFactory
     * @param Rejected $rejected
     */
    public function __construct(
        Context                     $context,
        Session                     $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface  $accountManagement,
        PageFactory                 $resultPageFactory,
        HolmbankOrderRepository     $holmbankOrderRepository,
        Rejected                    $rejected,
        Json                        $json,
        LoggerInterface             $logger,
        OrderRepositoryInterface    $orderRepository,
        JsonFactory                 $jsonResultFactory
    )
    {
        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement
        );
        $this->holmbankOrderRepository = $holmbankOrderRepository;
        $this->customerRepository = $customerRepository;
        $this->accountManagement = $accountManagement;
        $this->resultPageFactory = $resultPageFactory;
        $this->orderRepository = $orderRepository;
        $this->rejected = $rejected;
        $this->json = $json;
        $this->logger = $logger;
        $this->jsonResultFactory = $jsonResultFactory;
    }

    /**
     * Order success action
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        if ($this->getRequest()->isPost()) {
            return $this->handlePostEvent();
        }

        $orderId = $this->getRequest()->getParam('orderId');
        try {
            $holmOrder = $this->holmbankOrderRepository->getByOrderId($orderId);
        } catch (NoSuchEntityException $e) {
            $this->logger->error($e->getMessage());
            return $this->rejected->execute();
        }
        $displayStatus = $this->getRequest()->getParam('status');
        $loanStatus = $holmOrder->getOrderStatus();
        if ($loanStatus != Payment::PENDING_STATUS) {
            $displayStatus = $loanStatus;
        }

        switch ($displayStatus) {
            case Payment::APPROVED_STATUS:
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                return $resultRedirect->setPath('holmbank/payment/success');
            case Payment::REJECTED_STATUS:
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                return $resultRedirect->setPath('holmbank/payment/rejected');
            default:
                return $this->resultPageFactory->create();
        }
    }

    private function handlePostEvent(): \Magento\Framework\Controller\Result\Json
    {

        $reqData = $this->json->unserialize($this->getRequest()->getContent());

        $holmOrder = $this->holmbankOrderRepository->getByOrderId($reqData['orderId']);

        $this->logger->debug("POST request received for " . $holmOrder->getOrderId() . " with status " . $reqData["status"]);
        if ($reqData["status"] != $holmOrder->getOrderStatus()) {
            $holmOrder->setOrderStatus($reqData["status"]);
            $this->holmbankOrderRepository->save($holmOrder);
            $holmOrder = $this->holmbankOrderRepository->getByOrderId($reqData['orderId']);
        }

        $this->handleStoreOrder($holmOrder->getOrderId(), $holmOrder->getOrderStatus(), $reqData['contractNumber']);

        $response = $this->jsonResultFactory->create();
        $response->setHttpResponseCode(Response::STATUS_CODE_202);
        $response->setHeader("Content-Type", "application/json");
        $response->setStatusHeader(Response::STATUS_CODE_202);
        return $response->setData($holmOrder);
    }

    private function handleStoreOrder(int $orderId, string $status, ?string $contractNo)
    {
        $order = $this->orderRepository->get($orderId);
        switch ($status) {
            case Payment::APPROVED_STATUS:
                $order->setState(Order::STATE_PROCESSING);
                $order->setStatus(Order::STATE_PROCESSING);
                $order->addStatusToHistory($order->getStatus(), "Holm Bank hire purchase contract (".$contractNo.") paid out");
                break;
            case Payment::REJECTED_STATUS:
                $order->setState(Order::STATE_CANCELED);
                $order->setStatus(Order::STATE_CANCELED);
                $order->addStatusToHistory($order->getStatus(), 'Holm Bank hire purchase contract not signed');
                break;
            default:
                $order->setState(Order::STATE_PENDING_PAYMENT);
                $order->setStatus(Order::STATE_PENDING_PAYMENT);
                $order->addStatusToHistory($order->getStatus(), 'Holm Bank hire purchase contract pending');
        }
        $this->orderRepository->save($order);
        $this->logger->debug("Order " . $order->getEntityId() . " updated to " . $order->getStatus());
    }

    /**
     * Validate POST event
     *
     * @param RequestInterface $request
     * @return bool|null
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        $requestId = $request->getHeader("x-payment-link-req-id");
        if ($request->isGet()) {
            return true;
        }

        if (!$requestId) {
            $this->logger->error("x-payment-link-req-id is missing");
            return false;
        }

        $reqData = $this->json->unserialize($request->getContent());
        try {
            $order = $this->holmbankOrderRepository->getByOrderId($reqData['orderId']);
            if ($order->getRequestId() == $requestId && $order->getHolmbankOrderId() == $reqData['orderId']) {
                return true;
            }
            $this->logger->error("invalid x-payment-link-req-id for order " . $order->getOrderId());
        } catch (NoSuchEntityException $e) {
            $this->logger->error($e->getMessage());
        }
        return false;
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        $reqData = $this->json->unserialize($request->getContent());
        $error = "Holm Bank order " . $reqData['orderId'] . " not found for " . $reqData['storeId'];
        $response = $this->jsonResultFactory->create();
        $response->setHttpResponseCode(400);
        $response->setData(
            [
                "error" => $error
            ]);
        return new InvalidRequestException($response);
    }
}
