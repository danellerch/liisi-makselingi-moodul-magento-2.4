<?php

declare(strict_types=1);

namespace HolmBank\Payments\Api\Data;

interface HolmbankOrderInterface
{
    /**
     * String constants for property names
     */
    public const ENTITY_ID = "entity_id";
    public const HOLMBANK_ORDER_ID = "holmbank_order_id";
    public const CUSTOMER_ID = "customer_id";
    public const ORDER_ID = "order_id";
    public const REQUEST_ID = "request_id";
    public const ORDER_STATUS = "order_status";

    /**
     * Getter for HolmbankOrderId.
     *
     * @return string|null
     */
    public function getHolmbankOrderId(): ?string;

    /**
     * Setter for HolmbankOrderId.
     *
     * @param string|null $holmbankOrderId
     *
     * @return void
     */
    public function setHolmbankOrderId(?string $holmbankOrderId): void;

    /**
     * Getter for CustomerId.
     *
     * @return int|null
     */
    public function getCustomerId(): ?int;

    /**
     * Setter for CustomerId.
     *
     * @param int|null $customerId
     *
     * @return void
     */
    public function setCustomerId(?int $customerId): void;

    /**
     * Getter for CustomerId.
     *
     * @return int|null
     */
    public function getRequestId(): string;

    /**
     * Setter for CustomerId.
     *
     * @param int|null $customerId
     *
     * @return void
     */
    public function setRequestId(string $requestId): void;

    /**
     * Getter for OrderId.
     *
     * @return int|null
     */
    public function getOrderId(): ?int;

    /**
     * Setter for OrderId.
     *
     * @param int|null $orderId
     *
     * @return void
     */
    public function setOrderId(?int $orderId): void;

    /**
     * Getter for OrderStatus.
     *
     * @return string|null
     */
    public function getOrderStatus(): ?string;

    /**
     * Setter for OrderStatus.
     *
     * @param string|null $orderStatus
     *
     * @return void
     */
    public function setOrderStatus(?string $orderStatus): void;
}
