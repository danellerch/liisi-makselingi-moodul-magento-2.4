<?php

declare(strict_types=1);

namespace HolmBank\Payments\Api\Data;

interface HolmbankProductInterface
{
    public const ID = "entity_id";
    public const TYPE = "type";
    public const NAME = "name";
    public const LOGO = "logoUrl";

    public function getHolmBankProductType(): ?string;

    public function setHolmBankProductType(?string $productType): void;


    public function getHolmBankProductName(): ?string;

    public function setHolmBankProductName(?string $productName): void;


    public function getHolmBankProductLogo(): ?string;

    public function setHolmBankProductLogo(?string $productLogo): void;
}
