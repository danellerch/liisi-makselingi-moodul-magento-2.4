<?php

namespace HolmBank\Payments\Api;

use HolmBank\Payments\Api\Data\HolmbankOrderInterface;

interface HolmbankOrderRepositoryInterface
{
    /**
     * Save HolmbankOrder.
     *
     * @param HolmbankOrderInterface $order
     * @return void
     */
    public function save(HolmbankOrderInterface $order): void;

    /**
     * Get HolmbankOrder by order id.
     *
     * @param $orderId
     * @return HolmbankOrderInterface
     */
    public function getByOrderId($orderId): HolmbankOrderInterface;
}
